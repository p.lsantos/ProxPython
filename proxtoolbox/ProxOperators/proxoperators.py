# -*- coding: utf-8 -*-
"""
The "ProxOperators"-module contains various specific operators that do the actual calculations within the ProxToolbox.
"""

from numpy import conj, dot, empty, ones, sqrt, sum, zeros

__all__ = ["P_diag","P_parallel","magproj"]



class ProxOperator:
    """
    Generic interface for prox operators
    """

    def __init__(self, config):
        """
        Initialization method for a concrete instance
        
        Parameters
        ----------
        config : dict - Parameters to configure the algorithm
        """
        raise NotImplementedError("This is just an abstract interface")
    
    def work(self,u):
        """
        Applies a prox operator to some input data
        
        Parameters
        ----------
        u : array_like - Input data for the operator
        
        Returns
        -------
        array_like - Result of the operation
        """
        raise NotImplementedError("This is just an abstract interface")



def magproj(u, constr):
    """
    Projection operator onto a magnitude constraint
    
    Parameters
    ----------
    u : array_like - The function to be projected onto constr (can be complex)
    constr : array_like - A nonnegative array that is the magnitude constraint
    
    Returns
    -------
    array_like - The projection
    """
    modsq_u = conj(u) * u
    denom = modsq_u+3e-30
    denom2 = sqrt(denom)
    r_eps = (modsq_u/denom2) - constr
    dr_eps = (denom+3e-30)/(denom*denom2)
    
    return (1 - (dr_eps*r_eps)) * u



class P_diag(ProxOperator):
    """
    Projection onto the diagonal of a product space
    
    """
    
    def __init__(self,config):
        """
        Initialization
        
        Parameters
        ----------
        config : dict - Dictionary containing the problem configuration. It must contain the following mappings:
            
                'Nx': int
                    Row-dim of the product space elements
                'Ny': int
                    Column-dim of the product space elements
                'Nz': int
                    Depth-dim of the product space elements
                'dim': int
                    Size of the product space
        """
        self.n = config['Nx']; self.m = config['Ny']; self.p = config['Nz'];
        self.K = config['dim'];
    
    def work(self,u):
        """
        Projects the input data onto the diagonal of the product space given by its dimensions.
        
        """
        n = self.n; m = self.m; p = self.p; K = self.K;        
        
        if m == 1:
            tmp = sum(u,axis=1,dtype=u.dtype)
        elif n == 1:
            tmp = sum(u,axis=0,dtype=u.dtype)
        elif p == 1:
            tmp = zeros((n,m),dtype=u.dtype)
            for k in range(K):
                tmp += u[:,:,k]
        else:
            tmp = zeros((n,m,p),dtype=u.dtype)
            for k in range(K):
                tmp += u[:,:,:,k]
        
        tmp /= K
        
        if m == 1:
            return dot(tmp,ones((1,K),dtype=u.dtype))
        elif n == 1:
            return dot(ones((K,1),dtype=u.dtype),tmp)
        elif p == 1:
            u_diag = empty((n,m,K),dtype=u.dtype)
            for k in range(K):
                u_diag[:,:,k] = tmp
            return u_diag
        else:
            u_diag = empty((n,m,p,K),dtype=u.dtype)
            for k in range(K):
                u_diag[:,:,:,k] = tmp
            return u_diag





class P_parallel(ProxOperator):
    """
    Projection onto the diagonal of a product space
    
    """
    
    def __init__(self,config):
        """
        Initialization
        
        Parameters
        ----------
        config : dict - Dictionary containing the problem configuration. It must contain the following mappings:
            
                'Nx': int
                    Row-dim of the product space elements
                'Ny': int
                    Column-dim of the product space elements
                'Nz': int
                    Depth-dim of the product space elements
                'dim': int
                    Size of the product space
                'projs': sequence of ProxOperator
                    Sequence of prox operators to be used. (The classes, no instances)
        """
        self.n = config['Nx']
        self.m = config['Ny']
        self.p = config['Nz']
        self.K = config['dim']
        self.proj = []
        for p in config['projectors']:
            self.proj.append(p(config))
        
#        for p in config['projectors']:
#            self.proj.append(globals()[p](config))
#            pp = globals()[p]
#            self.proj.append(pp(config))

        
    def work(self,u):
        """
        Sequentially applies the projections of 'self.proj' to the input data.
        
        Parameters
        ----------
        u : array_like - Input
        
        Returns
        -------
        u_parallel : array_like - Projection
        """
        n = self.n; m = self.m; p = self.p; K = self.K; proj = self.proj

        if m == 1:
            u_parallel = empty((K,n),dtype=u.dtype)
            for k in range(K):
                u_parallel[k,:] = proj[k].work(u[k,:])
        elif n == 1:
            u_parallel = empty((m,K),dtype=u.dtype)
            for k in range(K):
                u_parallel[:,k] = proj[k].work(u[:,k])
        elif p == 1:
            u_parallel = empty((n,m,K),dtype=u.dtype)
            for k in range(K):
                u_parallel[:,:,k] = proj[k].work(u[:,:,k])
        else:
            u_parallel = empty((n,m,p,K),dtype=u.dtype)
            for k in range(K):
                u_parallel[:,:,:,k] = proj[k].work(u[:,:,:,k])
        
        return u_parallel


