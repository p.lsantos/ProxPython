# -*- coding: utf-8 -*-
"""
The "ProxOperators"-module contains all proximal operators that are already implemented in the ProxToolbox.
"""

from .proxoperators import *
from .bregman import *
__all__ = ["P_diag","P_parallel","magproj","bregProj"]
