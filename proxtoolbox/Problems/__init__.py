# -*- coding: utf-8 -*-
"""
The "Problems"-module contains all mathematical problems that can already be solved by the ProxToolbox.
"""

from .ptychography import *
from .sudoku import *
from .DRprotein import *

__all__ = ["Ptychography","Ptychography_NTT_01_26210","Sudoku","DRprotein"]
