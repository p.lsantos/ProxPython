#######
Credits
#######

Contributors:

	* Alexander Dornheim, Inst. for Numerical and Applied Math, Universität Göttingen (Matlab-Python port)
	* Robert Hesse, Inst. for Numerical and Applied Math, Universität Göttingen
	* Pär Mattson, Inst. for Numerical and Applied Math, Universität Göttingen (Ptychography)
	* `Matthew Tam <http://num.math.uni-goettingen.de/~mtam/>`_, Inst. for Numerical and Applied Math, Universität Göttingen (Ptychography)
	* Robin Wilke, Inst. for X-Ray Physics, Univesität Göttingen (Ptychography and Phase)
	* Alexander Stalljahn, Inst. for Numerical and Applied Math, Universität Göttingen (Sudoku)
	* Stefan Ziehe, Inst. for Numerical and Applied Math, Universität Göttingen (Matlab-Python port)

Funding:

	This has grown over the years and has been supported in part by:

		* NASA grant NGT5-66
		* the Pacific Institute for Mathematical Sciences (PIMS)
		* USA NSF Grant DMS-0712796
		* German DFG grant SFB-755

Bibliography:

	For a complete listing of papers with links go to `publications <http://vaopt.math.uni-goettingen.de/en/publications.php>`_.
