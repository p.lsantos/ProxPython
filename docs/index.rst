#####################
ProxToolbox |version|
#####################

In the following, we provide a tutorial, a manual and advanced descriptions for
the ProxToolbox.

.. toctree::
   :maxdepth: 1

   Tutorial/tutorial
   Manual/content
   Manual/addons
   Matlab-to-Python
   credits

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
