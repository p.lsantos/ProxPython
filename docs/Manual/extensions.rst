#################
Extending Samsara
#################

This will be a short guide on how to extend ProxPython with new functions.

Functions
=========

The ProxPython package is divided into the following modules:

...

Now that we know how the samsara package is structured we can look at
how to add a new function ourself:

#. Implement the function in the appropriate module.
#. Add the function as a new case to the appropriate wrapper in the
   same module.

   * If your new function has a parameter that is currently not
     provided by the wrapper, you will need to add it and update all
     calls to the wrapper.

#. If your function is an alternative to
   :func:`blah`,
   :func:`blah-blah` or
   :func:`blah-blah-blah`,
   then you will also need to add another case for your function in
   :func:`4blah`.

Your function is now implemented in ProxPython.  To use it, you will need
to import it into your driver and tell ProxPython to use it in the
options.  The following example illustrates these steps.

Example
-------

Let's say we want to add a new algorithm to ProxPython... 

Documentation
=============

This documentation is built using `Sphinx`_ with the
`numpydoc`_ extension. `This example`_ from the numpy project gives
a small overview and guideline on how to document your Python
functions.  To build the documentation you can invoke the included
`Makefile`:

.. _Sphinx: http://sphinx-doc.org/

.. _numpydoc: https://github.com/numpy/numpydoc

.. _This example: https://github.com/numpy/numpy/blob/master/doc/example.py

   >>> make html

or simply

   >>> make

to see all available build targets.
